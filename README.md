# Decoupled Mail Links

When decoupling drupal emails sent form the backend will still hold the base_url of the backend. This module replaces all base_urls with a configurable frontend url.

Please visit the settings form of the module and enter your frontend url:
/admin/config/system/decoupled_mail_links/settings

## Installation

Install like any other drupal module.
