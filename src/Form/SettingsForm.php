<?php

namespace Drupal\decoupled_mail_links\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Configure Leando decoupled mail links settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'decoupled_mail_links_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['decoupled_mail_links.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {


    global $base_url;

    // Display a page description.
    $form['description'] = [
      '#markup' => '<p>' . $this->t('This page allows you to configure settings which replaces the base url with your frontend_url.') . '</p>',
    ];

    $form['base_url_container'] = [
      '#id' => 'base_url_info',
      '#type' => 'details',
      '#title' => $this->t('BASE Url'),
      '#description' => $this->t('Defined Base Url'),
      '#open' => TRUE,
    ];

    $form['base_url_container']['base_url'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $base_url
    ];

    $form['frontend_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Frontend URL'),
      '#default_value' => $this->config('decoupled_mail_links.settings')->get('frontend_url'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!UrlHelper::isValid($form_state->getValue('frontend_url'), TRUE)) {
      $form_state->setErrorByName('frontend_url', $this->t('This must be a valid url.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('decoupled_mail_links.settings')
      ->set('frontend_url', $form_state->getValue('frontend_url'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
